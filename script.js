burger.addEventListener("click", () => {
  let burger = document.querySelector("#burger");
  let nav = document.querySelector("#nav");
  burger.classList.toggle("header__burger_active");
  nav.classList.toggle("navigation__active");
});

let submit = document.querySelector("#submit");
submit.addEventListener("click", (event) => {
  event.preventDefault();

  let inputName = document.querySelector("#name").value;
  let inputMail = document.querySelector("#email").value;
  let inputMessage = document.querySelector("#message").value;
  let blockError = document.querySelector("#error");
  let success = document.querySelector("#success");

  if (inputName === "" || inputMail === "" || inputMessage === "") {
    blockError.classList.add("form__fields-error_active");
  } else {
    success.classList.add("wrapper-success_active");
    blockError.classList.remove("form__fields-error_active");

    document.querySelector("#name").value = "";
    document.querySelector("#email").value = "";
    document.querySelector("#message").value = "";

    setTimeout(() => {
      success.classList.remove("wrapper-success_active");
    }, 3000);
  }
});
